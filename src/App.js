import React from 'react';
import Catalog from './components/Catalog';


function App() {

  return (
    <Catalog />
  );
}

export default App;
